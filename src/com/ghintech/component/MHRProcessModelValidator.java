package com.ghintech.component;

import java.util.List;
import org.adempiere.base.event.AbstractEventHandler;
import org.adempiere.base.event.IEventTopics;
import org.compiere.model.PO;
import org.compiere.model.Query;
import org.compiere.util.CLogger;
import org.eevolution.model.I_HR_Process;
import org.eevolution.model.X_HR_Process;
import org.osgi.service.event.Event;
import org.eevolution.model.MHRMovement;
public class MHRProcessModelValidator extends AbstractEventHandler {
	

		private static CLogger log = CLogger.getCLogger(MHRProcessModelValidator.class);

		@Override
		protected void initialize() {
			registerTableEvent(IEventTopics.DOC_AFTER_COMPLETE, I_HR_Process.Table_Name);
		}

		@Override
		protected void doHandleEvent(Event event) {

			PO po = getPO(event);
			String type = event.getTopic();
			log.info(po.get_TableName() + " Type: " + type);
			
			
			
			if (po.get_TableName().equals(I_HR_Process.Table_Name) && type.equals(IEventTopics.DOC_AFTER_COMPLETE)) {
				X_HR_Process payroll = (X_HR_Process) po;
				List<MHRMovement> listmovement = new Query(po.getCtx(), MHRMovement.Table_Name, "HR_Process_ID=?", 
						po.get_TrxName()).setParameters(payroll.get_ID()).list();
				
				for (MHRMovement mov : listmovement) {
					mov.setProcessed(true);
					mov.saveEx();
				}

				
			}

		}
}

		
